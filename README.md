# Alexa Birthdays

A sample skill to demonstrate the following [Alexa Skills Kit](https://github.com/alexa/alexa-skills-kit-sdk-for-nodejs) features:
- DynamoDB Session Saving
- Dialog Management
- Notifications (coming soon)
- Account Linking (coming soon)

Use the [ASK CLI](https://github.com/amznlabs/ask-cli) to create a new skill and this repository as an example for creation. For Dialog Management features, please make note of the interaction model in `models/en-US.json`.

# FAQ and Tips

## Deploying changes
`ask deploy --no-wait` is your friend!

Also, be sure to `npm update` if you've copied! Or you may get an error like this:
```
Unable to import module 'index': Error
at Function.Module._resolveFilename (module.js:469:15)
```

## Errors with Dialog Management
If your session is ending and you get a SessionEndedRequest that looks like the following:

```
    "request": {
        "type": "SessionEndedRequest",
        "requestId": "amzn1.echo-api.request.6a84c091-73e6-47dd-b840-bbbbbbbbbb",
        "timestamp": "2017-10-31T06:42:13Z",
        "locale": "en-US",
        "reason": "ERROR",
        "error": {
            "type": "INVALID_RESPONSE",
            "message": "The requested skill has not been configured to support dialogs yet. Please update the skill's configuration from the Developer Console"
        }
    }
```

Then you most likely need to update your interaction model (e.g. `models/en-US.json` to support Dialog Management for those slots).

## Access Denied for DynamoDB
If you receive the following error, then you need to grant `AWSLambdaInvocation-DynamoDB` to the IAM role setup for your skill.

```
{
    "errorMessage": "Error fetching user state: AccessDeniedException: User: arn:aws:sts::1111111111111:assumed-role/ask-lambda-birthdays/ask-custom-bdays-default is not authorized to perform: dynamodb:GetItem on resource: arn:aws:dynamodb:us-east-1:1111111111111:table/Birthdays",
    "errorType": "Error",
    "stackTrace": [
        "ValidateRequest.attributesHelper.get (/var/task/node_modules/alexa-sdk/lib/alexa.js:163:33)",
        "Response.<anonymous> (/var/task/node_modules/alexa-sdk/lib/DynamoAttributesHelper.js:39:25)",
        "Request.<anonymous> (/var/task/node_modules/aws-sdk/lib/request.js:364:18)",
        "Request.callListeners (/var/task/node_modules/aws-sdk/lib/sequential_executor.js:105:20)",
        "Request.emit (/var/task/node_modules/aws-sdk/lib/sequential_executor.js:77:10)",
        "Request.emit (/var/task/node_modules/aws-sdk/lib/request.js:683:14)",
        "Request.transition (/var/task/node_modules/aws-sdk/lib/request.js:22:10)",
        "AcceptorStateMachine.runTo (/var/task/node_modules/aws-sdk/lib/state_machine.js:14:12)",
        "/var/task/node_modules/aws-sdk/lib/state_machine.js:26:10",
        "Request.<anonymous> (/var/task/node_modules/aws-sdk/lib/request.js:38:9)"
    ]
}
```