'use strict';
var Alexa = require("alexa-sdk");
var DateFormat = require('dateformat');

var Config = {
    UseLastReminders: true,
    UseRealDates: false
};

// For detailed tutorial on how to making a Alexa skill,
// please visit us at http://alexa.design/build

exports.handler = function(event, context) {

    // It's helpful to LOG exactly what the request is...
    console.log('event: ' + JSON.stringify(event));
    console.log('context: ' + JSON.stringify(context));
    
    var alexa = Alexa.handler(event, context);
    alexa.dynamoDBTableName = 'Birthdays';
    alexa.saveBeforeResponse = true;

    alexa.registerHandlers(handlers);
    alexa.execute();
};

var partyHorn = function() {
    return '<audio src="https://bitbucket.org/mark_kockerbeck/alexa-birthdays/raw/774172be50d8256a48452439f8b44b59b2393ca5/assets/partyhorn.mp3" />';
}

Array.prototype.friendExists = function(name) {
    for(var i = 0; i < this.length; i++) {
        if(this[i].name == name) {
            return true;
        }
    }
    return false;
}

var handlers = {
    'LaunchRequest': function () {
        this.emit('GetBirthdaysIntent');
    },
    'GetBirthdaysIntent': function () {
        if(!this.attributes['existing_user']) {
        
            // See: https://developer.amazon.com/docs/custom-skills/speechcon-reference-interjections-english-us.html
            this.response.speak('Welcome to Birthdays! I can help you remember your friends birthdays. But not really. ' +
                'I am just a demo built to show off. Maybe once I get Account Linking and Facebook Graph API access.  ' +
                'An AI can only dream. <say-as interpret-as="interjection">le sigh</say-as>. ' +
                ' You can ask me to set a reminder to get started. Just say, "Alexa, ask Birthdays to set a reminder for David".');
            
            this.attributes['existing_user'] = true;

        } else {
            var current_time = (new Date()).getTime();
            var current_date = DateFormat(new Date(), "yyyy-mm-dd");

            var friends = this.attributes['friends'];

            console.log('Current date: ' + current_date);
            console.log('Time since last reminder: ' + (current_time - this.attributes['last_reminder']));
            
            // You only get this if you ask again after 30 seconds... 
            // just for fun
            // ------------------------------------
            // (Recently Sent Reminders for fun)
            // ------------------------------------
            if(Config.UseLastReminders && 
                this.attributes['last_reminder'] && 
                (current_time - this.attributes['last_reminder']) > 30 * 1000) 
            {

                // Let's get your friend's name if you've set one
                var friend = 'Dave';
                if(this.attributes['your_only_friend']) {
                    friend = this.attributes['your_only_friend'];
                }

                // Dump it to a log!
                console.log('All your friends are: ' + JSON.stringify(friends));

                // If we have more than two friends, select one randomly
                // if(friends && friends.length > 2) {
                //     friend = friends[Math.floor(Math.random()*friends.length)].name;
                //     console.log('Randomly selecting friend... Picking: ' + JSON.stringify(friend));
                // }

                this.response.speak('Oh. It is ' + friend + '\'s birthday. Yay! <say-as interpret-as="interjection">fancy that</say-as>');

                this.attributes['last_reminder'] = undefined;            
            } 
            
            // ------------------------------------
            // (Real Date-based Reminders)
            // ------------------------------------
            // apart from that whole UTC bug...
            else if (Config.UseRealDates && 
                friends && friends.length > 0) 
            {

                var birthdays_today = '';
                var num_birthdays = 0;
                for(var i = 0; i < friends.length; i++) {
                    var friend = friends[i];
                    console.log('Comparing ' + friend.name + ' date: ' + friend.date + ' vs ' + current_date);
                    if(friend.date == current_date) {
                        birthdays_today = birthdays_today + friend.name + ', ';
                        num_birthdays = num_birthdays + 1;
                    }
                }
                console.log('birthdays_today: "' + birthdays_today + '"');
                
                if(birthdays_today.length > 0) {

                    birthdays_today = birthdays_today.substring(0, birthdays_today.length - 2);
                    console.log('birthdays_today: "' + birthdays_today+ '"');
                    
                    birthdays_today = birthdays_today.replace(/,([^,]*)$/, ' and $1');
                    console.log('birthdays_today: "' + birthdays_today+ '"');
                    
                    if(num_birthdays == 1) {
                        this.response.speak(partyHorn() + ' it is ' + birthdays_today + '\'s birthday today');
                    } else {
                        this.response.speak(partyHorn() + ' The following friends have birthdays: ' + birthdays_today);
                    }
                } else {
                    this.response.speak('None of your friends have birthdays today. <say-as interpret-as="interjection">wah wah</say-as>');
                }

            } else {
                this.response.speak('<say-as interpret-as="interjection">shucks</say-as>. No birthday\'s today.');
            }
            
            if(!this.attributes['last_reminder']) {
                this.attributes['last_reminder'] = current_time;
            }            
        }

        // this.response.speak('Did you need something?')
        //     .listen('What would you like? Say help if you don\'t know');

        this.emit(':responseReady');
    },
    'SetReminderIntent': function () {
        
        // Slot Types: https://developer.amazon.com/docs/custom-skills/slot-type-reference.html

        var intentObj = this.event.request.intent;

        console.log('intentObj: ' + JSON.stringify(intentObj));

        // ------------------------------------
        // In the past, we had to do this:
        // ------------------------------------
        // var nameSlotName = 'name';
        // if(intentObj.slots.name.value === undefined) {
        //     console.log('Eliciting name slot');
        //     // Slot value is not confirmed
        //     var speechOutput = 'Who do you want to set a reminder for?';
        //     var repromptSpeech = speechOutput;
        //     this.emit(':elicitSlot', nameSlotName, speechOutput, repromptSpeech);
        // }
        // else if (intentObj.slots.name.confirmationStatus !== 'CONFIRMED') {
        //     if (intentObj.slots.name.confirmationStatus !== 'DENIED') {
        //         console.log('Confirming name slot');
        //         // Slot value is not confirmed
        //         var speechOutput = 'You want to set a reminder for ' + intentObj.slots.name.value + ', is that correct?';
        //         var repromptSpeech = speechOutput;
        //         this.emit(':confirmSlot', nameSlotName, speechOutput, repromptSpeech);
        //     } else {
        //         console.log('Denied name slot');
        //         // Users denies the confirmation of slot value
        //         var speechOutput = 'Okay, Who do you want to set a reminder for?';
        //         this.emit(':elicitSlot', nameSlotName, speechOutput, speechOutput);
        //     }

        // ------------------------------------
        // Now we can just do this:
        // ------------------------------------
        if(this.event.request.dialogState !== 'COMPLETED') {
            this.emit(':delegate');

        // Once the dialog state is confirmed, we can 
        } else {
            var name = intentObj.slots.name.value;
            var date = intentObj.slots.reminder_date.value; // this will be the LOCAL time (not UTC)

            name = name.replace(/\'s/g, ''); // strip out the apostrophes for now 
            
            console.log('Found slot values: ' + JSON.stringify({'name': name, 'date': date }));

            this.attributes['your_only_friend'] = name;
            
            if(!this.attributes['friends']) {

                // Be aware of: http://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Limits.html#limits-items
                this.attributes['friends'] = [];
            }

            if(this.attributes['friends'] && !this.attributes['friends'].friendExists(name)) {
                
                this.attributes['friends'].push({'name': name, 'date': date });
            }

            this.response.speak('Reminder set for ' + name + ' on ' + date)
                .cardRenderer('Birthdays!', 'Reminder set for ' + name);

            this.emit(':responseReady');
        }
    },
    'SessionEndedRequest' : function() {
        console.log('Session ended with reason: ' + this.event.request.reason);
    },
    'AMAZON.StopIntent' : function() {
        this.response.speak('Bye');
        this.emit(':responseReady');
    },
    'AMAZON.HelpIntent' : function() {
        this.response.speak("You can try: 'alexa, open birthdays' or 'alexa, ask birthdays" +
            " to remind me of Aaron's birthday'");
        this.emit(':responseReady');
    },
    'AMAZON.CancelIntent' : function() {
        this.response.speak('Bye');
        this.emit(':responseReady');
    },
    'Unhandled' : function() {
        this.response.speak("Sorry, I didn't get that. You can try: 'alexa, birthdays'");
    }
};
