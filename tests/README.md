# Testing your Skill

There are many ways to test your skill including:

- Unit tests (e.g. Mocha)
- Direct Lambda Invocations (e.g. `aws lambda invoke --function-name YourSkill`)
- ASK CLI Invoke (see: **invoke.sh**)
- SMAPI HTTP Requests (see: [SMAPI Skill Invocation API](https://developer.amazon.com/docs/smapi/skill-invocation-api.html))

## TIP

If the ASK CLI tool is not invoking your skill, be sure to see [.ask/config](.ask/config) for the "regions" section of the skill manifest. You likely need to specify your ARN lambda endpoint there as well.