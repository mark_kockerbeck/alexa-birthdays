#!/bin/bash

for f in *.mp3
do
    echo $f
    cp "$f" "$f-2"
    rm "$f"
    ffmpeg -i "$f-2" -ac 2 -codec:a libmp3lame -b:a 48k -ar 16000 "$f"
    rm "$f-2"
done
